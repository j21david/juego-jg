//
//  GameModel.swift
//  GameGR1JG
//
//  Created by Jose Gonzalez on 15/11/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import Foundation
class GameModel {
    var puntaje:Int = 0
    var ronda:Int = 1
    var objetivo:Int?
    
    init() {
        setObjectivo()
    }
    
    
    func setObjectivo() {
        objetivo  = Int(arc4random_uniform(99)+1)
        
    }
    
    func jugar(valorIntento: Int){
        ronda += 1
        puntaje += calcularPuntaje(valorIntento: valorIntento)
        setObjectivo()
    }
    
    func reiniciar(){
        puntaje = 0
        ronda = 1
        setObjectivo()
    }
    
    func calcularPuntaje(valorIntento:Int) -> Int{
        
        let diferencia = abs(valorIntento - objetivo!)
        
        
        switch diferencia {
        case 0:
            return 100
        case 1...3:
            return 75
            
        case 4...10:
            return 50
        case 333...3333: 
            return 50
        default:
            return 0
            
        }
        
        
    }
}

