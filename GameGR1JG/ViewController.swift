//
//  ViewController.swift
//  GameGR1JG
//
//  Created by Jose Gonzalez on 15/11/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    let gameModel = GameModel()
    
    
    @IBOutlet weak var puntajeLabel: UILabel!
    @IBOutlet weak var rondaLabel: UILabel!
    @IBOutlet weak var objetivoLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objetivoLabel.text = "\(gameModel.objetivo ?? 0)"
        puntajeLabel.text = "\(gameModel.puntaje)"
        rondaLabel.text = "\(gameModel.ronda)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    @IBAction func jugarButtonPressed(_ sender: Any) {
        
        gameModel.jugar(valorIntento: Int(round(slider.value)))
        setValores()
    }
    
    @IBAction func reiniciarButtonPressed(_ sender: Any) {
    
        gameModel.reiniciar()
        setValores()
    }
    
    func setValores(){
        objetivoLabel.text = "\(gameModel.objetivo ?? 0)"
        puntajeLabel.text = "\(gameModel.puntaje)"
        rondaLabel.text = "\(gameModel.ronda)"
        
        
    }
    
    
}

